const Gif = ({ gifUrl, title }) => {
  return (
    <div>
      <p>{title}</p>
      <img src={gifUrl} alt={title} width="128" height="128" />
    </div>
  );
};

export default Gif;

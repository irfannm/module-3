import { useState } from "react";
import axios from "axios";
import Gif from "../../components/Gif";
import "./Search.css";

const Search = () => {
  const [gif, setGif] = useState();
  const [query, setQuery] = useState("");

  const API_KEY = process.env.REACT_APP_GIPHY_API_KEY;
  const limit = 12;

  const getGif = async () => {
    const gifs = await axios
      .get(
        `https://api.giphy.com/v1/gifs/search?q=${query}&api_key=${API_KEY}&limit=${limit}`
      )
      .then((response) => response)
      .catch((error) => error);
    setGif(gifs.data.data);
  };

  return (
    <div>
      <div>
        <input type="text" onInput={(e) => setQuery(e.target.value)} />
        <button
          onClick={() => {
            getGif();
          }}
        >
          Search
        </button>
        <div className="result">
          {gif !== undefined &&
            gif.map((gifs) => {
              return (
                <Gif
                  key={gifs.id}
                  title={gifs.title}
                  gifUrl={gifs.images.fixed_width.url}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
};
export default Search;

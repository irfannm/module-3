import "./App.css";
import { useState } from "react";
import Search from "./pages/Search";

function App() {
  return (
    <>
      <Search />
    </>
  );
}

export default App;
